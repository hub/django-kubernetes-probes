from django.urls import path, include

urlpatterns = [
    path('', include('django_kubernetes_probes.urls')),
]
