import logging

from django.test import TestCase
from django.urls import reverse
from django.db import connection
from django.contrib.auth import get_user_model

from tests import models

logger = logging.getLogger(__name__)


class ProbeTestCase(TestCase):
    def drap_table(self, model):
        with connection.cursor() as cursor:
            cursor.execute(f"DROP TABLE {model._meta.db_table}")

    def test_probe_ready(self):
        self.assertEqual(200, self.client.get(reverse("probes:probe_ready")).status_code)

    def test_probe_ready_without_user(self):
        self.drap_table(get_user_model())
        self.assertEqual(500, self.client.get(reverse("probes:probe_ready")).status_code)

    def test_probe_ready_without_amodel(self):
        self.drap_table(models.AModel)
        self.assertEqual(500, self.client.get(reverse("probes:probe_ready")).status_code)

    def test_probe_alive(self):
        self.assertEqual(200, self.client.get(reverse("probes:probe_alive")).status_code)

    def test_probe_alive_without_user(self):
        self.drap_table(get_user_model())
        self.assertEqual(500, self.client.get(reverse("probes:probe_alive")).status_code)

    def test_probe_alive_without_amodel(self):
        """
        for alive-ness we only look at user, other missing table in the db does not cause an error
        """
        self.drap_table(models.AModel)
        self.assertEqual(200, self.client.get(reverse("probes:probe_alive")).status_code)
