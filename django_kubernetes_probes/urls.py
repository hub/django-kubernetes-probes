from django.urls import path

from . import views

app_name = "probes"
urlpatterns = [
    ##############################
    # url(r'^$', views.index, name='home'),
    ##############################
    path("probe_alive/", views.is_alive, name="probe_alive"),
    path("probe_ready/", views.is_ready, name="probe_ready"),
]
