from django.contrib.auth import get_user_model
from django.http import HttpResponse


def is_ready(request):
    from django.db import OperationalError
    import django.apps

    try:
        for model_class in django.apps.apps.get_models():
            if not model_class._meta.managed:
                continue  # we don't test not managed models
            model_class.objects.first()
        return HttpResponse(status=200)
    except OperationalError:
        pass
    return HttpResponse(status=500)


def is_alive(request):
    from django.db import OperationalError

    try:
        get_user_model().objects.exists()
        return HttpResponse(status=200)
    except OperationalError:
        pass
    return HttpResponse(status=500)
