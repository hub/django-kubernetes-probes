from django.apps import AppConfig


class ProbesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "django_kubernetes_probes"
    label = "probes"
