============
django-kubernetes-probes
============

django-kubernetes-probes is a Django app to conduct web-based polls. For each
question, visitors can choose between a fixed number of answers.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "polls" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...,
        "django_kubernetes_probes",
    ]

2. Include the polls URLconf in your project urls.py like this::

    path('', include('django_kubernetes_probes.urls')),

4. Start the development server.

5. Visit the ``/probe_alive/`` URL to see that you app is alive.
